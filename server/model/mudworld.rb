
require 'yaml'
require 'mudutils'
require 'mudserver'

# @author Bart Pelle
class MudWorld

  attr_accessor :rooms, :server

  # @param server [MudServer] reference to the server
  def initialize(server)
    @server = server
    @rooms = YAML.load_file('config/rooms.yaml')

    MudUtils.log "Loaded #{@rooms.size} MUD rooms from YAML file."
  end

  # Check if a room exists
  # @param x [Fixnum] x-coordinate of the room
  # @param y [Fixnum] y-coordinate of the room
  def room_exists?(x, y)
    room_at(x, y) != nil
  end

  # Find a room at specific coordinates
  # @param x [Fixnum] x-coordinate of the room
  # @param y [Fixnum] y-coordinate of the room
  def room_at(x, y)
    @rooms.select { |room| room.x == x and room.y == y}.first
  end

end