require 'mudconstants'
require 'yaml'
require 'socket'
require 'model/mudworld'

class MudPlayer

  attr_accessor :state, :name, :password, :cmdproc, :info, :connection, :world, :room

  # @param connection [TCPSocket] the tcp socket this player is connected with
  # @param world [MudWorld] the world this player lives in
  def initialize(connection, world)
    @connection = connection
    @state = PlayerState::FRESH
    @world = world
  end

  # Sends a line, breaking the line after sending.
  # @param str [String] plain string to send, line break following
  def send(str)
    @connection.send "#{str}\r\n", 0
  end

  # Sends a line, breaking the line after sending.
  # @param message [Object] plain object to send, line break following
  def <<(message)
    @connection.send "#{message.to_s}\r\n", 0
  end

  # Sends a sequence of data without breaking the line.
  # @param raw [Object] plain object to send, without any line breaks following
  def &(raw)
    @connection.send "#{raw.to_s}", 0
  end

  # Called when the player joins the world (aka logged in)
  def start
    @state = PlayerState::IN_GAME

    # Find the room we're in
    @room = @world.room_at(@info.pos_x, @info.pos_y)
    unless @room # Report if we're in a nil-room
      self << 'You were standing in a nonexistant place. You\'ve been moved to the Town Hall of South Velocity.'
      @info.pos_x = 0
      @info.pos_y = 0
      @room = @world.room_at 0, 0
    end

    self << "Welcome to the world of Velocia, #{@name}."
  end

  # Simple proc to welcome our player
  def welcome
    self << '____________________________________'.bold
    self << '| __     __   _            _        |'.bold
    self << '| \ \   / /__| | ___   ___(_) __ _  |'.bold
    self << '|  \ \ / / _ \ |/ _ \ / __| |/ _` | |'.bold
    self << '|   \ V /  __/ | (_) | (__| | (_| | |'.bold
    self << '|    \_/ \___|_|\___/ \___|_|\__,_| |'.bold
    self << '|        And the Legend of DragonKK |'.bold
    self << ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'.bold
    self << ' '
    self << 'Welcome to Velocia. Would you please ' + 'login'.light_blue.bold + ' or ' + 'register'.light_blue.bold + '?'
  end

  #
  # Convenience methods
  #

  def x
    @info.pos_x
  end

  def y
    @info.pos_y
  end

  def x= val
    @info.pos_x = val
  end

  def y= val
    @info.pos_y = val
  end

end