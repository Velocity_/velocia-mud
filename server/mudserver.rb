require 'socket'
require 'yaml'
require 'colorize'

require 'mudlogic'
require 'mudconfig'
require 'mudutils'
require 'model/mudplayer'
require 'data/mudplayerdata'
require 'data/mudroom'
require 'model/mudworld'

class MudServer

  attr_accessor :world, :connections

  def initialize
    ip = MudConfig.get('listen_ip')
    port = MudConfig.get('listen_port')

    @world = MudWorld.new(self)
    @connections = []

    @server = TCPServer.open ip, port

    # Start our logic thread
    MudLogic.new self

    MudUtils.log "Listening on #{ip}:#{port}."
    accept_connections
  end

  def accept_connections
    loop do
      player = MudPlayer.new @server.accept, @world
      @connections.push player

      MudUtils.log "New connection: #{player.connection.addr[3]} (#{@connections.size} connected in total)"
      player.welcome
    end
  end

end