class MudConfig

  CONFIG_FILE = 'config/config.yaml'

  def self.load
    @data = Psych.load_file CONFIG_FILE
  end

  # @param key [String] key to use to find entries
  def self.get(key)
    # If the data isn't loaded yet we must load it of course
    unless @data
      load
    end

    @data[key]
  end

end