require 'model/mudplayer'
require 'colorize'

# @param player [MudPlayer] the player issuing this command
def look(player, params)
  unless player.state == PlayerState::IN_GAME
    player << 'You are not logged in.'.light_red
    return
  end

  player.room.look player
end


def money(player, params)
  unless player.state == PlayerState::IN_GAME
    player << 'You are not logged in.'.light_red
    return
  end

  player << "You currently carry #{player.info.coins.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse} coins in your pouch.".bold.yellow
end