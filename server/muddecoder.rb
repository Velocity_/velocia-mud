require 'mudutils'
require 'mudcommands'

class MudDecoder

  # @param server [MudServer] the server running
  # @param player [MudPlayer] the player who's going to be decoded
  def self.decode(server, player)
    begin
      cmd = player.connection.recv_nonblock(2048)
    rescue Exception => _
      return
    end

    # See if we either disconnected, or have enough.
    if cmd.empty?
      MudUtils.log "Socket #{player.connection} disconnected."
      server.connections.delete player
      return
    end

    # Clean it...
    cmd.strip!

    MudUtils.log "Incoming command: #{cmd}."

    # Process the command
    begin
      MudCommands.issue(player, cmd)
    rescue Exception => e
      MudUtils.log "Error processing command: #{e}. Type: #{e.class}. Trace: #{e.backtrace.join("\n")}"
    end
  end

end