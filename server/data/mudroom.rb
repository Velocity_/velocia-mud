require 'model/mudplayer'

class MudRoom

  attr_accessor :x, :y, :description

  # @param player [MudPlayer] the player who's looking around
  def look(player)
    player << @description.bold.yellow
  end

end