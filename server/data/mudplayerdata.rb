require 'yaml'

class MudPlayerData

  attr_accessor :name, :password, :pos_x, :pos_y, :coins

  # @param name [String] name of the player
  # @param password [String] password of the player
  # @param pos_x [Fixnum] room x position of this player
  # @param pos_y [Fixnum] room y position of this player
  def initialize(name, password, pos_x, pos_y)
    @name = name
    @password = password
    @pos_x = pos_x
    @pos_y = pos_y
  end

  # @return [String]
  def serialize
    YAML::dump self
  end

  # @param name [String] the name of the player to load
  def self.load(name)
    charfile = File.join(MudConfig.get('character_directory'), '%s.yaml' % name.downcase)
    puts charfile

    # Check if the file exists first
    return nil unless File.exist?(charfile)

    # Load it!
    return YAML.load_file charfile
  end

end