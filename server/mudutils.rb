require 'mudconfig'

class MudUtils

  LOG_FILE = MudConfig.get('log_file')
  TIME_FORMAT = MudConfig.get('log_time_format')

  # @param str [String] the text to print to the console
  def self.log(str)
    time = Time.now.strftime TIME_FORMAT
    puts "[#{time}] #{str}"

    if LOG_FILE != nil
      IO.write(LOG_FILE, "[#{time}] #{str}\r\n", File.size?(LOG_FILE))
    end
  end

  def self.clear_console
    system 'clear' or system 'cls'
  end

end