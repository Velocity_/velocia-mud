require 'mudutils'
require 'mudconfig'
require 'model/mudplayer'
require 'muddecoder'

class MudLogic

  LOGIC_INTERVAL = MudConfig.get('logic_interval') / 1000.0 # Milliseconds -> seconds

  # @param server [MudServer] the server intiating this logic thread
  def initialize(server)
    @server = server

    t = Thread.new { logic_loop }
    t.run
  end

  def logic_loop
    loop do
      begin
        # We don't need to process everything non-stop, give the cpu some rest...
        sleep LOGIC_INTERVAL

        MudUtils.log "Processing #{@server.connections.size} players..."

        @server.connections.each do |player|
          MudDecoder.decode @server, player
        end
      rescue IO::EAGAINWaitReadable
        # ignored
      rescue Exception => e
        MudUtils.log "Error while processing logic: #{e} #{e.class}"
      end
    end
  end

end