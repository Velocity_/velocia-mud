require 'colorize'
require 'mudconstants'
require 'data/mudplayerdata'
require 'digest'
require 'model/mudplayer'
require 'commands/general'

class MudCommands

  @map = nil
  @sha1 = Digest::SHA1.new

  # @param player [MudPlayer] the player who issued the command
  # @param cmd [String] the raw input string
  def self.issue(player, cmd)
    original = cmd
    split = cmd.split(' ')
    cmd = split.first

    # Attempt to call a stored command proc
    if player.cmdproc != nil
      # Reset cmdproc if the result was true
      player.cmdproc = nil if player.cmdproc.call(player, cmd, split, original)

      return
    end

    if @map == nil
      @map = {'help' => method(:help)}
      @map['login'] = method(:login)
      @map['look'] = method(:look)
      @map['coins'] = @map['money'] = method(:money)
    end

    meth = @map[cmd]

    if meth != nil
      meth[player, split]
    else
      player << 'Unknown command '.light_red + ':('.light_green
    end
  end

  # @param player [MudPlayer] the player invoking this help command
  # @param params [Array] array of strings holding parameters
  def self.help(player, params)
    if player.state == PlayerState::FRESH
      player << 'Please ' + 'LOGIN'.bold.light_blue + ' or ' + 'REGISTER'.bold.light_blue + '.'
      return
    end

    player << 'I cannot help you. Sorry.'.light_red.bold
  end

  # @param player [MudPlayer] the player invoking this help command
  # @param params [Array] array of strings holding parameters
  def self.login(player, params)
    if player.state == PlayerState::FRESH
      player << 'What is your username?'.bold.light_blue

      player.cmdproc = @login_proc
    else
      player << 'You are already logged into the game. Please '.light_red + 'LOGOUT'.bold.light_blue + ' first.'.light_red
    end
  end

  # The proc used to accept the username
  @login_proc = Proc.new { |player, _, _, full|
    data = MudPlayerData.load(full)

    if data
      player.cmdproc = @password_proc

      # Before we let the user enter his password, we make foreground & background black. This hides input.
      player.info = MudPlayerData.load(full)
      player.name = player.info.name
      player & "Hello, #{player.name}. Could you type your password?\r\n\033[30m\033[40m                                    \r"
    else
      player << "Hmm, there's no user called #{full}. Did you mean to ".light_red + 'register'.bold.light_blue + '?'.light_red
    end

    false
  }

  # The proc used to accept the password
  @password_proc = Proc.new { |player, _, _, full|
    # SHA1 the password
    @sha1 << full
    full = @sha1.hexdigest
    @sha1.reset

    # Reset colors & clear password line
    player & "\033[0m\033[1M\033[1A\033[1M"

    if player.info.password == full
      player.start # Welcome, player!
      true
    else
      player.cmdproc = @password_proc
      player & "Sorry, #{player.name}, but that's not your password. Can you type it again?\r\n\033[30m\033[40m                                    \r"
      false
    end
  }

end